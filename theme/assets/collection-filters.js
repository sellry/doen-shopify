/* eslint-disable func-style */
(function() {
  const data = JSON.parse(document.querySelector('script[data-collection]').innerHTML);
  const filterDefinitions = JSON.parse(document.querySelector('script[data-filter-definitions]').innerHTML);
  const dynamicSettings = window.DynamicCollections;

  console.log('DYNAMIC SETTINGS', dynamicSettings, Boolean(dynamicSettings[data.handle]), data.handle);

  console.warn('Collection Filters!!!!!', data);

  console.log('Filter Definitions', filterDefinitions);
  // console.log('First Item', data.items[0].title);
  function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  function removeSize(src) {
    return src.replace(/(_x1024|_1024x1024)/i, '');
  }

  function removeProtocol(path) {
    return path.replace(/http(s)?:/, '');
  }

  function getSizedImageUrl(src, size) {
    if (size === null) {
      return src;
    }

    if (size === 'master') {
      return removeProtocol(src);
    }

    const match = src.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i);

    if (match) {
      const prefix = src.split(match[0]);
      const suffix = match[0];

      return removeProtocol(`${prefix[0]}_${size}${suffix}`);
    } else {
      return null;
    }
  }

  function sizeImage(src, item) {
    const master = removeSize(src);

    return getSizedImageUrl(master, '700x');
  }

  const makeid = (length = 7) => {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  };

  const intersection = (a, b) => {
    const s = new Set(b);
    return [...new Set(a)].filter((x) => s.has(x));
  };

  const uniqueElementsBy = (arr, fn) =>
  arr.reduce((acc, v) => {
    if (!acc.some((x) => fn(v, x))) { acc.push(v); }
    return acc;
  }, []);


  function sortAssociated(value) {
    const shopifyOptions = {
      manual: 'manual',
      'best-selling': 'best-selling',
      'price-ascending': 'low-high',
      'price-descending': 'high-low',
    };

    const themeOptions = {
      manual: 'manual',
      'best-selling': 'best-selling',
      'low-high': 'price-ascending',
      'high-low': 'price-descending',
    };

    return shopifyOptions[value] || themeOptions[value] || 'manual';
  }

  function updateUrlParameters(parameters) {
    const currentParameters = getJsonFromUrl();

    const newParameters = {
      ...currentParameters,
      ...parameters,
    };

    console.log('UPDATE PARAMETERS', newParameters);

    const final = Object.keys(newParameters).reduce((final, key) => {
      if (!newParameters[key] || (Array.isArray(newParameters[key]) && newParameters[key].length === 0)) { return final; }

      console.log('Update URL', newParameters[key], newParameters[key].value);

      const value = (Array.isArray(newParameters[key])) ? newParameters[key].reduce((finalString, value) => {
        let currentString = finalString;

        console.log('Adding Value', value, currentString);
        if (value.collections) {

          value.collections = (Array.isArray(value.collections)) ? value.collections: [value.collections];
          // currentString += `${value.collections.join('_')}-${value.value},`;
          value.collections.forEach((collection) => {
            currentString += `${collection}-${value.value},`;
          });
        } else {
          currentString += `${value.replace(/&/g, '__')},`;
        }

        return currentString;
      }, '') : newParameters[key];

      console.log('Adding ', Array.isArray(newParameters[key]), newParameters[key], value);

      return `${final}${key}=${value}&`;
    }, '?');

    window.history.pushState(
      {},
      null,
      final,
    );
  }

  function getJsonFromUrl(url) {
    if (!url) { url = location.href; }
    const question = url.indexOf('?');
    let hash = url.indexOf('#');
    if (hash == -1 && question == -1) { return {}; }
    if (hash == -1) { hash = url.length; }
    const query = question == -1 || hash == question + 1 ? url.substring(hash)
    : url.substring(question + 1, hash);
    const result = {};
    query.split('&').forEach((part) => {
      if (!part) { return; }
      part = part.split('+').join(' '); // replace every + with space, regexp-free version
      const eq = part.indexOf('=');
      let key = eq > -1 ? part.substr(0, eq) : part;
      const val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';
      const from = key.indexOf('[');
      if (from == -1) { result[decodeURIComponent(key)] = val; } else {
        const to = key.indexOf(']', from);
        const index = decodeURIComponent(key.substring(from + 1, to));
        key = decodeURIComponent(key.substring(0, from));
        if (!result[key]) { result[key] = []; }
        if (!index) { result[key].push(val); } else { result[key][index] = val; }
      }
    });
    return result;
  }

  function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }

  const chunk = (arr, size) => Array.from({length: Math.ceil(arr.length / size)}, (v, i) =>
    arr.slice(i * size, i * size + size));


  Vue.directive('autoclick', {
    inserted: (el) => {
      function handleClick() {
        const button = Array.from(el.children).find(
              (el) => el.nodeName === 'BUTTON',
          );

        console.log('Going to Click', button);
        if (button) {
          button.click();
        }
      }

      function handleIntersect(entries, observer) {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            console.log('Handle Click Now');
            handleClick();
            // observer.unobserve(el);
          }
        });
      }

      function createObserver() {
        const options = {
          root: null,
          rootMargin: '0px 0px 100px 0px',
          threshold: '0.1',
        };

        console.log('Attaching Observer to', el);
        const observer = new IntersectionObserver(handleIntersect, options);
        observer.observe(el);
      }

      console.warn('AUTO CLICKER ESTABLISH??');

      if (window.IntersectionObserver) {
        createObserver();
      }
    },
  });


  Vue.directive('autoscroll', {
    inserted: (el) => {

      const landingParams = getJsonFromUrl();

      function scrollToElement(id) {
        const link = el.querySelector(`#product-${id}`);

        console.log('Scroll To Element', link, id);

        if (!link) {
          return;
        }

        const element = el;
        const headerOffset = document.querySelector('header').clientHeight + 20;
        const elementPosition = element.getBoundingClientRect().top;
        const offsetPosition = elementPosition - headerOffset;

        updateUrlParameters({
          i: false,
        });

        window.scrollTo({
          top: offsetPosition,
          // behavior: 'smooth',
        });
        window.scrolled = true;

        console.warn('SCROLL TO', el, id, offsetPosition);
      }

      if (landingParams.page && landingParams.i) {
        const id = landingParams.i;

        scrollToElement(id);
      }

    },
  });


  Vue.directive('lazyload', {
    inserted: (el) => {
      function loadImage() {
        const imageElement = Array.from(el.children).filter(
          (el) => el.nodeName === 'IMG',
          );

        if (imageElement.length > 0) {
          imageElement.forEach((img) => {
            img.addEventListener('load', () => {
              setTimeout(() => {
                el.classList.add('loaded');
                img.classList.add('observed');
              }, 250);
            });
            img.addEventListener('error', () => console.log('error'));
            img.src = sizeImage(img.dataset.url, el);
            img.removeAttribute('data-url');
          });
        }
      }

      function handleIntersect(entries, observer) {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            loadImage();
            observer.unobserve(el);
          }
        });
      }

      function createObserver() {
        const options = {
          root: null,
          threshold: '0',
        };

        const observer = new IntersectionObserver(handleIntersect, options);
        observer.observe(el);
      }
      if (window.IntersectionObserver) {
        createObserver();
      } else {
        loadImage();
      }
    },
  });

  Vue.component('sort', {
    template: document.querySelector('script[vue-template-sort-option]').innerHTML,
    props: {},
    data() {
      return {
        selected: data.default_sort,
        open: false,
        options: [
          {
            label: 'Best Selling',
            value: 'best-selling',
          },
          {
            label: 'Price: Low To High',
            value: 'low-high',
          },
          {
            label: 'Price: High To Low',
            value: 'high-low',
          },
          {
            label: 'In Stock',
            value: 'in-stock',
          },
          {
            label: 'No Sort',
            value: 'manual',
          },
        ],
      };
    },
    computed: {
      activeDescendant() {
        const activeIndex = [...this.options].findIndex((option) => option.value === this.selected);

        return `option-${activeIndex}`;
      },
    },
    methods: {
      handleKeyDown(event) {
        switch (event.key) {
          case 'Enter':
            this.handleTriggerClick();
        }
        console.log('Trigger KeyDown');
      },
      handleMouseLeave() {
        this.open = false;
      },
      handleTriggerClick() {
        this.$emit('sort-toggle', {
          open: !this.open,
        });
        this.open = !this.open;
      },
      selectOption(event) {
        const value = event.target.getAttribute('data-value');

        this.selected = value;

        this.$emit('filter-update', {
          type: 'sort',
          value,
          add: true,
        });

      },
    },
    created() {
      this.$root.$on('filter-reset', () => {
        this.selected = data.default_sort;
      });
    },
    mounted() {
      if (getJsonFromUrl().sort) {
        this.selected = getJsonFromUrl().sort;
      }

      this.$parent.$on('filter-reset', () => {
        this.selected = data.default_sort;
      });
      console.log('Sort Mounted');
    },
  });

  const oldCategorySettings = [
    {
      label: 'Apparel',
      testType: 'collection',
      collections: ['dresses', 'tops', 't-shirts', 'knitwear', 'outerwear', 'sleepwear', 'slips', 'celebration'],
      values: [],
    },
    {
      label: 'Adults',
      testType: 'exclusionCollection',
      collections: [],
      values: [],
      exclusionCollections: ['kids'],
    },
    {
      label: 'Accessories',
      testType: 'collection',
      collections: ['accessories'],
      values: [],
    },
    {
      label: 'Denim',
      testType: 'collection',
      collections: ['denim', 'bottoms'],
      values: [],
    },
    {
      label: 'footwear',
      testType: 'collection',
      collections: ['footwear'],
      exclusionTags: ['SOCKS', 'style-socks'],
      values: [],
    },
    {
      label: 'Shoes',
      testType: 'collection',
      collections: ['footwear'],
      exclusionTags: ['SOCKS', 'style-socks'],
      values: [],
    },
    {
      label: 'socks',
      testType: 'tags',
      tags: ['SOCKS', 'style-socks'],
      collections: ['footwear'],
      exclusionCollections: ['kids'],
      values: [],
    },
    {
      label: 'kids',
      testType: 'collection',
      collections: ['kids'],
      exclusionTags: ['SOCKS', 'style-socks'],
      values: [],
    },
  ];

  Vue.component('filter-option', {
    template: document.querySelector('script[vue-template-filter-option]').innerHTML,
    props: {
      label: {
        type: String,
        default: '',
        required: false,
      },
      standard: {
        type: Boolean,
        default: false,
      },
      id: {
        type: String,
        default: makeid(6),
      },
      option: {
        type: String,
        default: '',
      },
    },
    data() {
      return {
        filtersOpen: (window.innerWidth > 768),
        categories: filterDefinitions,
        open: false,
        options: [],
        selected: [],
      };
    },
    computed: {
      activeDescendant() {
        return 'option-0';
        const activeIndex = [...this.options].findIndex((option) => option.value === this.selected);

        return `option-${activeIndex}`;
      },
    },
    methods: {
        templateRows(label, value) {
        const {handle} = data;

        if(handle === 'bottoms') label = 'bottoms';

        if ((handle === 'new-arrivals' || handle === 'all' || handle === 'bottoms') && /bottoms/ig.test(label)) {
          console.log('Handle Qualified');

            if (isNumber(value) && !this.hide(value, label.toLowerCase())) {
              return `grid-column: ${Number(value) < 13 ? '1' : '2'}`
            } else {
              return `grid-column: 3;` 
            }
          // const numberCount = [...category.values].reduce((count, value) => {
          //   console.log('Number?', value, isNumber(value));
          //   if (isNumber(value) && !this.hide(value, category.label.toLowerCase())) {
          //     count++;
          //   }

          //   return count;
          // }, 0);
          // console.log('Calculate rows', category, numberCount);

          // return `repeat(${numberCount}, 1fr)`;
        }

        console.log('Template Row?');

        if(handle === 'pregnancy-nursing') {
          if (isNumber(value) ) {
            return `grid-column: 1`
          } else {
            return `grid-column: 2;`
          }
        }

        return '';
      },
      noDigits(str) {

        return str.replace(/\d/g,'');
      },
      isActiveCategory(category) {
        const dynamicCategories = dynamicSettings[data.handle];

        // console.log('Check Category Settings', dynamicCategories, category.toLowerCase());

        if (!dynamicCategories) { return false; }

        const isInSettings = dynamicCategories.includes(category.toLowerCase());

        // console.log('Category is in Settings?', isInSettings, category.toLowerCase());

        return isInSettings;
      },
      fetchValues(page = 1) {
        return fetch(`/collections/${data.handle}?view=sizes&page=${page}`)
          .then((res) => res.json())
          .catch(() => console.log('Size Error Fetch'));
      },
      handleKeyDown(event) {
        switch (event.key) {
          case 'Enter':
            this.handleTriggerClick();
        }
        console.log('Trigger KeyDown');
      },
      handleMouseLeave(event) {
        if (window.innerWidth < 786) {
          console.log('Mouse Leave Handler', event.target);
          return;
        }

        this.open = false;
      },
      handleTriggerClick() {

        console.log('Mouse Clicked Filters');

        this.open = !this.open;
      },
      hide(value, category = data.handle) {
        const collectionHiddens = window.theme.filter_settings.hidden_sizes[data.handle];
        const isArray = Array.isArray(collectionHiddens);

        if (isArray) {
          const hiddenValues = window.theme.filter_settings.hidden_sizes[category] || [];

          const hidden = hiddenValues.includes(value);

          return hidden ? 'hide' : '';
        } else {

          // console.log('Hide?', window.theme.filter_settings.hidden_sizes, collectionHiddens, category, value);
          if (!collectionHiddens) { return ''; }

          const hiddenValues = collectionHiddens[category] || [];
          // const hiddenValues = window.theme.filter_settings.hidden_sizes[category] || [];

          const hidden = hiddenValues.includes(value);
          // console.warn('HIDE', value, category, hiddenValues);
          return hidden ? 'hide' : '';
        }

        // console.log('Hide?', window.theme.filter_settings.hidden_sizes, collectionHiddens, category, value);
        // if (!collectionHiddens) { return ''; }

        // const hiddenValues = collectionHiddens[category] || [];
        const hiddenValues = window.theme.filter_settings.hidden_sizes[category] || [];

        const hidden = hiddenValues.includes(value);
        console.warn('HIDE', value, category, hiddenValues);
        return hidden ? 'hide' : '';
      },
      optionActive(value, category) {
        // console.log('Option Active', value, category, this.selected, this.standard);

        if (this.standard) {
          return this.selected.includes(value);
        } else {
          const matchTest = new RegExp(category, 'i');
          const isPresent = [...this.selected].find((item) => {
            return item.value === value && Boolean(item.label.match(matchTest));
          });

          // console.log('Finding Active?', matchTest, this.selected, isPresent);

          return Boolean(isPresent);
        }
      },
      specialSelectOption(event) {
        const value = event.target.getAttribute('data-value');
        const label = event.target.getAttribute('data-category');

        const associatedSizes = this.getMaskedAssociations(value, label);

        // Need category for payload of collections.
        const category = [...this.categories].find((item) => item.label === label);

        const add = ([...this.selected].findIndex((item) => item.label === label && item.value === value) === -1);


        console.log('Association', label, associatedSizes, category);
        const payload = category.testType === 'tags' ? category.tags : (category.testType === 'exclusionCollection') ? category.label : category.collections;

        this.$emit('filter-update', {
          type: 'size',
          value: [value].concat(associatedSizes),
          payload,
          testType: category.testType,
          add: Boolean(add),
        });

        if (add) {
          [value].concat(associatedSizes).forEach((v) => {
            this.selected.push({
              value: v,
              label,
            });
          });
        } else {
          const removalIndex = [...this.selected].findIndex((item) => item.label === label && item.value === value);

          this.selected = [...this.selected].filter((item, index) => {
            return removalIndex !== index;
          });
        }


        console.warn('Special Select Option', value, this.selected);
      },
      selectOption(event) {
        const value = event.target.getAttribute('data-value');
        const add = ![...this.selected].find((item) => item === value);

        const associatedSizes = this.getMaskedAssociations(value);
        const valueWithAssociated = [value].concat(associatedSizes);

        console.warn('Associated Sizes', associatedSizes);

        this.$emit('filter-update', {
          type: 'size',
          value: valueWithAssociated,
          add,
        });


        if (add) {
          const newSelected = [...this.selected, value].concat(associatedSizes);

          console.log('New Option Selected', [...this.selected, value].concat(associatedSizes));

          // newSelected.push(value);
          // newSelected = newSelected.concat(associatedSizes);

          this.selected = [...new Set(newSelected)];

        } else {
          const removeWithAssociated = [value].concat(associatedSizes);

          const newValues = [...this.selected].filter((item) => !removeWithAssociated.includes(item));
          this.selected = [...newValues];
        }

        console.warn('After Update', add, this.selected);

      },
      getMaskedAssociations(value, label = null) {
        let associatedMaskedSizes = window.theme.filter_settings.mask_sizes[data.handle];

        if(!associatedMaskedSizes && label) {
          associatedMaskedSizes = window.theme.filter_settings.mask_sizes[label.toLowerCase()];
        }
        console.log('Mask Sizes', value);

        if (!associatedMaskedSizes) { return []; }

        const associated = associatedMaskedSizes[value] || [];

        return associated;

        console.log('Filtering Sizes', sizes, associatedMaskedSizes, sizeWithMasked);

      },
      sortPageData(data) {
        const categories = [...this.categories];
        const order = window.theme.filter_settings.orders[this.label.toLowerCase()];

        // Exclude sock products from values.
        const products = [...data];

        if (this.standard) {
          const sizeOptions = [...products].reduce((sizes, product) => [...new Set(sizes.concat(product.sizes))], []);
          const currentSizeOptions = [...this.options].reduce((all, opt) => all.concat([opt.value]), []);
          // console.log('Setting Size Options', sizeOptions, currentSizeOptions);

          // [ numbers[], strings[] ]
          const partitionedValues = [...new Set([...sizeOptions, ...currentSizeOptions])].reduce((final, value) => {
            if (isNumber(value)) {
              final[0].push(value);
            } else {
              final[1].push(value);
            }

            return final;
          }, [[], []]);

          const sortedNumbers = partitionedValues[0].sort((valueA, valueB) => valueA - valueB);
          const sortedStrings = partitionedValues[1].sort((valueA, valueB) => {
            const testA = new RegExp(`^${valueA}`, 'i');
            const testB = new RegExp(`^${valueB}`, 'i');
            const aIndex = order.findIndex((item) => testA.test(item));
            const bIndex = order.findIndex((item) => testB.test(item));

            return aIndex - bIndex;
          });


          this.options = [...new Set([...sortedNumbers, ...sortedStrings])].map((size) => {
            return {
              label: size,
              value: size,
            };
          });

          // console.log('Standard Values', this.options);

        } else {

          const sockProducts = products.filter((product) => product.tags.includes('SOCKS') || product.tags.includes('style-socks'));
          // console.log('Setting Category Values', products[0], categories[5], this.categories);

          // console.warn('SOCK PRODUCTS', sockProducts);
          products.forEach((product) => {

            const categoriesWithIndex = categories.map((category, index) => {
              return {
                ...category,
                index,
              };
            });

            const isSock = product.tags.includes('SOCKS') || product.tags.includes('style-socks');

            // console.log('Categories w Index', categoriesWithIndex);

            const productCategories = categoriesWithIndex.filter((category) => {
              const {exclusionTags, testType} = category;

              if (testType === 'collection') {
                return intersection(category.collections, product.collections).length > 0 && intersection(category.exclusionCollections, product.collections).length === 0 && intersection(exclusionTags, product.tags).length === 0;
              }

              if (testType === 'tags') {
                if (category.exclusionCollections && category.exclusionCollections.length > 0) {
                  return intersection(category.tags, product.tags).length > 0 && intersection(category.exclusionCollections, product.collections).length === 0;
                }

                return intersection(category.tags, product.tags).length > 0;
              }

              if (testType === 'exclusionCollection') {
                return intersection(category.exclusionCollections, product.collections).length === 0 && intersection(exclusionTags, product.tags).length === 0;
              }
            });


            // if (isSock) {
            //   console.log('IS SOCK', productCategories, isSock);
            // }

            // const categoryIndex = categories.findIndex((category) => {
            //   const {exclusionTags, label, testType} = category;
            //   // console.warn('Categories', label, testType, category);

            //   if (testType === 'collection') {
            //     return intersection(category.collections, product.collections).length > 0 && intersection(exclusionTags, product.tags).length === 0;
            //   }

            //   if (testType === 'tags') {
            //     return intersection(category.tags, product.tags).length > 0;
            //   }

            //   if (testType === 'exclusionCollection') {
            //     console.log('Exclusion Collectin', intersection(category.exclusionCollections, product.collections));
            //     return intersection(category.exclusionCollections, product.collections).length === 0;
            //   }

            // });

            // console.log('Product Categories',productCategories);

            if (productCategories.length > 0) {
              const collator = new Intl.Collator('en', { numeric: true, sensitivity: 'base' });
              productCategories.forEach((category) => {
                const currentValues = category.values;
                const productSizes = product.sizes || [];
                // const availableSizes = productSizes.filter((size) => product.variants[size]);
 
                const withNext = currentValues.concat(productSizes).sort((a,b) => collator.compare(a,b));

                categories[category.index].values = [...new Set(withNext)].sort((valueA, valueB) => {
                  const testA = new RegExp(`^${valueA}`, 'i');
                  const testB = new RegExp(`^${valueB}`, 'i');
                  const aIndex = order.findIndex((item) => testA.test(item));
                  const bIndex = order.findIndex((item) => testB.test(item));

                  return aIndex - bIndex;
                });
              });
            }

            // if (categoryIndex > -1) {
            //   const currents = categories[categoryIndex].values;
            //   const together = currents.concat(product.sizes);

            //   categories[categoryIndex].values = [...new Set(together)].sort((valueA, valueB) => {
            //     const testA = new RegExp(`^${valueA}`, 'i');
            //     const testB = new RegExp(`^${valueB}`, 'i');
            //     const aIndex = order.findIndex((item) => testA.test(item));
            //     const bIndex = order.findIndex((item) => testB.test(item));

            //     return aIndex - bIndex;
            //   });
            //   // console.log('Setting Categories', categories[categoryIndex], [...new Set(together)]);
            // }

            // console.log('Special Size Cateogires', getJsonFromUrl());

          });

          console.log('After Category Product Loop', this.categories);
        }

        return true;

        // console.log('Sort Page Data', data);
      },
    },
    created() {
      this.$root.$on('filter-reset', () => {
        this.selected = [];
      });

      this.$root.$on('filter-toggle', (open) => {
        console.log('Toggle Filter Toggle', open);

        this.filtersOpen = open;


        console.log('Filters Open', this.filtersOpen);
      });
    },
    async mounted() {

      const pages = Math.ceil(data.total_count / 75);
      let index = 0;

      console.log('Re-mount Filter Options', this.options, this.selected, getJsonFromUrl().sizes);


      if (getJsonFromUrl().sizes && !this.standard) {
        const appliedSizes = getJsonFromUrl().sizes.split(',');
        // console.log('Applied Sizes', appliedSizes);
        appliedSizes.forEach((size) => {
          let [label, value, ...backupValue] = size.split('-');
          if (!value) { return; }

          if (label === 't') {
            label = 't-shirts';
            value = backupValue;
          }

          if (backupValue) {
            value = `${value}-${backupValue.join('-')}`;
          }

          if (label === '') { return; }

          const categoryForFilter = this.categories.find((cat) => cat.collections.includes(label));

          console.log('Trying to match label', {categoryForFilter, label, categories: this.categories});
          const alreadySelected = this.selected.find((selected) => selected.label === categoryForFilter.label && selected.value === value);

          console.log('Reassigning Cate', this.selected, value, label, categoryForFilter);
          if (!alreadySelected) {

            console.log('Adding', categoryForFilter, value);

            this.selected.push({
              label: categoryForFilter.label,
              value,
            });
          }
        });
      }

      while (index < pages) {
        console.warn('Fetch Page?', index, pages);
        const pageData = await this.fetchValues(index + 1);

        await this.sortPageData(pageData);
        index++;
      }

      console.log('Mounted Selected', this.selected, this.options);

      if (getJsonFromUrl().sizes && this.standard) {
        this.selected = [...getJsonFromUrl().sizes.split(',')].filter((val) => val !== '');
        console.log('Standard Update', this.selected, getJsonFromUrl().sizes, [...getJsonFromUrl().sizes]);
      }

      console.log('Filter Option Mounted', this.standard, pages, this.categories);
    },
  });


  Vue.component('tag-filter', {
    template: document.querySelector('script[vue-template-tag-option]').innerHTML,
    props: {
      label: {
        type: String,
        default: '',
        required: false,
      },
      identifier: {
        type: String,
        default: '',
        required: true,
      },
    },
    data() {
      return {
        selected: [],
        open: false,
        options: [],
      };
    },
    computed: {
      activeDescendant() {
        return 'option-0';
        const activeIndex = [...this.options].findIndex((option) => option.value === this.selected);

        return `option-${activeIndex}`;
      },
    },
    methods: {
      isActiveOption(value) {
        return this.selected.includes(value);
      },
      handleKeyDown(event) {
        switch (event.key) {
          case 'Enter':
            this.handleTriggerClick();
        }
        console.log('Trigger KeyDown');
      },
      handleMouseLeave() {
        this.open = false;
      },
      handleTriggerClick() {

        this.open = !this.open;
      },
      selectOption(event) {
        const value = event.target.getAttribute('data-value');

        this.$emit('filter-update', {
          type: 'tag',
          value,
          add: !this.isActiveOption(value),
        });

        if (this.isActiveOption(value)) {
          const newValues = [...this.selected].filter((selected) => selected !== value);
          this.selected = [...newValues];
        } else {
          this.selected.push(value);
        }


        console.log('Fire Selected Option', value);
        // this.selected = value;
      },
    },
    created() {
      this.$root.$on('filter-reset', () => {
        this.selected = [];
      });
    },
    mounted() {
      let order = window.theme.filter_settings.orders[this.identifier.replace('-', '')] || [];
      const identifyingTest = new RegExp(`^${this.identifier}`, 'i');

      if (this.identifier === 'style-') {
        console.log('STYLE IDENTIFIER ORDER', this.identifier, window.theme.filter_settings.orders.style[data.handle]);
        order = window.theme.filter_settings.orders.style[data.handle] || [];
      }


      console.log('Style Order', order);

      const values = [...data.all_tags].filter((tag) => {
        return identifyingTest.test(tag);
      }).filter((tag) => {
        return order.indexOf(tag.replace(identifyingTest, '')) !== -1;
      }).map((tag) => {
        return {
          value: tag,
          label: tag.replace(identifyingTest, ''),
        };
      })
.sort((tagA, tagB) => {
  return order.indexOf(tagA.value.replace(identifyingTest, '')) - order.indexOf(tagB.value.replace(identifyingTest, ''));
});

      this.options = [...values];

      if (getJsonFromUrl().tags) {
        const applicableHistoryTags = [...getJsonFromUrl().tags.split(',')].filter((tag) => {
          return identifyingTest.test(tag);
        }).map((tag) => tag.replace(/__/g, '&'));

        console.log('Url Tags', this.options, getJsonFromUrl(), getJsonFromUrl().tags, applicableHistoryTags, identifyingTest);

        this.selected = [...applicableHistoryTags];
        console.log('Set Selected', this.selected);
      }
    },
  });

  Vue.component('grid-item', {
    template:  (window.preview_mode) ? document.querySelector('script[vue-preview-template-grid-item]').innerHTML : document.querySelector('script[vue-template-grid-item]').innerHTML,
    props: {
      product: {
        type: Object,
        required: true,
      },
      index: {
        type: Number,
        required: false,
      },
      currency: {
        type: String,
        required: false,
	  },
      page: {
        type: Number,
        required: false,
      },
    },
    data() {
      return {
        disableLink: false,
      };
    },
    computed: {
      mainImage() {

      },
      isWaitlist() {
        const {tags} = this.product;
	
		return tags.includes('waitlist');
      },
      isPreorder() {
        const {tags} = this.product;

        const preOrderTagPresent = tags.find((tag) => /preorder/i.test(tag));

        if (!preOrderTagPresent) {
          return false;
        }

        const productWidePreorder = (preOrderTagPresent.indexOf(':') === -1);

        return productWidePreorder;
      },
    },
    methods: {
      hasTag(tag) {
        return this.product.tags.includes(tag);
      },
      productLinkHandler(event) {
        event.preventDefault();
        if (window.innerWidth <= 786 && this.disableLink) {
          return;
        }

        const id = event.currentTarget.getAttribute('data-product-id');

        console.log('Product Link Handler', window.history.state);

        updateUrlParameters({
          page: this.product.page,
          i: id,
        });

        console.warn('Product Link Handler', event.currentTarget, this.product);

        window.location.href = event.currentTarget.getAttribute('href');
      },
      formatMoneyUSD(value) {
        return window.Currency.formatMoney(value);
      },
      formatMoney(value) {
        const currentCurrency = this.currency || 'USD';
        const format = window.Currency.moneyFormats[currentCurrency].money_format;
        const converted = (currentCurrency === 'USD') ? value : Currency.convert(value, 'USD', currentCurrency);

        // console.log('Formatting Money', currentCurrency, window.Currency.formatMoney(converted, format), format);

        return window.Currency.formatMoney(converted, format);
      },
      variantClasses(variant) {
        const soldoutClass = (this.variantCheck(variant)) ? 'product-grid-rollover-soldout' : '';

        return `product-grid-rollover-container rollover-wrapper-size ${soldoutClass}`;
      },
      variantCheck(variant) {
        return (variant.inventory_quantity <= variant.preorder.limit * -1);
      },
      returnSize(variant) {
        const sizeOption = [...this.product.options].find((option) => /size/i.test(option.name));

        // console.log('Return size', { variant, sizeOption }, this.product, this.product.options)
        if (!sizeOption) { return ''; }

        return variant[`option${sizeOption.position}`];
      },
    },
    created() {
      this.$root.$on('link-disable', (disable) => {
        console.log('Item Link Disable?', disable);

        if (window.innerWidth <= 786) {
          this.disableLink = disable;
        }
      });
    },
    mounted() {
      console.log('Product Item Mounted', this.currency, this.page);
    },
  });

  const defaultFilters = {
    inStock: false,
    sizes: [],
    tags: [],
    sort: null,
  };

  const historyState = getJsonFromUrl() || {};

  // console.log('Assigning new Filters', historyState);

  Object.keys(historyState).forEach((key) => {
    if (!Object.keys(defaultFilters).includes(key)) { return; }

    console.log('Is Key Array?', key, Array.isArray(defaultFilters[key]), defaultFilters[key]);

    if (Array.isArray(defaultFilters[key])) {
      // defaultFilters[key] = historyState[key].split(',');

      console.log('FIRST IF?', (key === 'sizes' && (dynamicSettings[data.handle])));

      if (key === 'sizes' && (dynamicSettings[data.handle])) {
        defaultFilters[key] = [];
        const allSizes = historyState[key].split(',');
        allSizes.forEach((size) => {
          let [collection, value, ...backup] = size.split('-');
          console.log('Looping Sizes', size);

          if (!value) { return; }

          if (collection === 't') {
            collection = 't-shirts';
            value = backup;
          }

          console.log('Creating Value', value, backup);
          if (backup.length > 0) {
            value = `${value}-${backup.join('-')}`;
          }

          const obj = {
            collections: [collection],
            value,
          };


          const inFiltersAlready = defaultFilters[key].find((filter) => filter.value === value);

          console.log('Trying To Add', inFiltersAlready, obj);

          if (inFiltersAlready) {
            defaultFilters[key] = defaultFilters[key].map((filter) => {
              const filterCollections = filter.collections;
              const filterValue = filter.value;

              // console.log('Mapping new values', filterValue === value, value, filterValue);
              if (filterValue === value) {
                const collections = filterCollections.concat([collection]);

                return {
                  value,
                  collections,
                };
              } else {
                return filter;
              }
            });
          } else {
            defaultFilters[key].push(obj);
          }
        });
      } else {
        defaultFilters[key] = historyState[key].split(',').map((val) => val.replace(/__/g, '&')).filter((value) => value !== '');
        // console.log('Set', key, defaultFilters[key]);
      }
    } else if (key === 'sort') {
      defaultFilters[key] = historyState[key];
    } else {
      defaultFilters[key] = historyState[key].split(',').filter((value) => value !== '');
    }
  });

  console.log('After Update Default Filters', defaultFilters);

  window.CollectionApp = new Vue({
    el: '#main-collection-body',
    data: {
      bestSellingOrder: [],
      previewLoading: false,
      standard: !dynamicSettings[data.handle],
      allProducts: [...data.items],
      tempItems: [...data.items],
      gatherFinished: false,
      appliedFilters: defaultFilters,
      currency: window.Currency.currentCurrency || 'USD',
      filtersOpen: (window.innerWidth >= 768),
      pagination: {
        currentPage: data.pagination.current,
        initialPage: data.pagination.current,
        initialOffset: data.pagination.current !== 1,
        loading: false,
        maxPages: data.pagination.pages,
        perPage: 40,
      },
    },
    computed: {
      items() {
        const {initialPage, initialOffset, perPage, currentPage} = this.pagination;

        console.log('Items?', this.allProducts.length, this.gatherFinished, initialPage, currentPage);
        if (!this.gatherFinished && this.filtersActive()) {
          return [];
        }

        // console.log('Before Position filter', this.appliedFilters, this.filtersActive(), this.allProducts.length, this.allProducts);
        // Set Items & Remove Mis-Matched by positioning if no initialOffset
        let items = (initialOffset && !this.gatherFinished) ? [...this.allProducts].filter((item, i) => {
          return item.position === ((i + 1) + data.pagination.basis);
        }) : [...this.allProducts].filter((item, i) => (window.preview_mode) ? true : item.position === i + 1);

        // filter by tag
        // items = [...items].filter((item) => intersection(item.tags, data.exclusions).length === 0);

        console.log('Items before filter', { items });
        if (this.filtersActive()) {
          items = this.filterItems([...items]);
        }
        console.log('After Filter', items.length);

        const itemsWithoutExclusions = [...items].filter((item) => intersection(item.tags, data.exclusions).length === 0);

        console.log('Items after Exclusions', itemsWithoutExclusions.length);

        const byPage = chunk([...itemsWithoutExclusions], perPage);

        if (!this.gatherFinished) {
          // If not all products, add empty pages.
          for (let i = 0; i < (initialPage - 1); i++) {
            byPage.unshift([]);
          }
        }

        const qualifyingPages = [...byPage].filter((page, index) => {
          return (this.filtersActive()) ? (index < currentPage) : ((initialPage - 1) <= index) && (index < currentPage);
        }).reduce((all, page, index) => {
          const pageReAssigned = page.map((item) => {
            const newItem = item;
            newItem.page = index + 1;
            return newItem;
          });

          return all.concat(pageReAssigned);
        }, []);

        console.log('First Product', { byPage, items }, qualifyingPages[0], qualifyingPages.length);


        // filter by tag
        // qualifyingPages = [...qualifyingPages].filter((item) => intersection(item.tags, data.exclusions).length === 0);

        // if(qualifyingPages.length === 0) {
        //  qualifyingPages = [...byPage].filter((page, index) => {
        //     return (this.filtersActive()) ? (index < currentPage + 1) : ((initialPage - 1) <= index) && (index < currentPage + 1);
        //   }).reduce((all, page) => all.concat(page), []);

        //   qualifyingPages = [...qualifyingPages].filter((item) => intersection(item.tags, data.exclusions).length === 0);
        // }

        console.log('After Qualifying Pages', qualifyingPages);
        return qualifyingPages;
      },
    },
    methods: {
      addProducts(page) {
        const {products} = page;
        const togetherBatch = uniqueElementsBy(this.tempItems.concat(products), (a, b) => a.id === b.id);

        const sortedBatch = togetherBatch.sort((a, b) => a.position - b.position);
        this.tempItems = [...sortedBatch];
        // this.allProducts = [...sortedBatch];
        // console.log('Add Products', products);
      },
      clearAllFilters(event) {
        event.preventDefault();

        this.appliedFilters = {
          inStock: false,
          sizes: [],
          tags: [],
          sort: null,
        };

        // reset pagination as well.
        this.pagination.currentPage = 1;
        this.pagination.initialPage = 1;
        updateUrlParameters({
          page: 1,
        });

        this.updateBrowserFilterState();
        this.$emit('filter-reset');
      },
      async fetchBestSellingOrder() {
        const pages = Math.ceil(data.total_count / 500);
        let final_order = [];
        let page = 0;

        while (page < pages) {
          try {
            const {order} = await fetch(`/collections/${data.handle}?view=best-selling&sort_by=best-selling&page=${page}`).then((res) => res.json());
            final_order = final_order.concat(order);
          } catch (error) {
            console.warn('Catch Errors', error);
          }
          page++;
        }

        this.bestSellingOrder = [...final_order];
      },
      fetchPage(page = 1) {
        return fetch(`/collections/${data.handle}?view=filter-data&page=${page}`)
          .then((res) => res.json())
          .then((data) => this.addProducts(data))
          .catch(() => console.log('Size Error Fetch'));
      },
      filtersActive() {
        const {appliedFilters} = this;

        if (appliedFilters.sort) { return true; }

        if (appliedFilters.inStock) { return true; }

        if (appliedFilters.tags.length > 0 || appliedFilters.sizes.length > 0) { return true; }

        return false;
      },
      filterItems(items) {
        const {appliedFilters} = this;

        console.log('Going to Filter Items', appliedFilters);

        let products = [...items];
        if (appliedFilters.inStock) {
          products = [...products].filter((item) => item.available);
        }

        console.log('Amount prior to size filter', products.length, this.standard);
        if (appliedFilters.sizes.length > 0) {
          if (this.standard) {
            products = this.filterSize(products);
          } else {
            products = this.filterSizeSpecial(products);
          }
        }
        console.log('Amount before tags filter', products.length);

        if (appliedFilters.tags.length > 0) {
          const tagsBreakdown = [...appliedFilters.tags].reduce((final, tag) => {
            const [key, value] = tag.split('-');

            if(!final[key]) final[key] = [];

            final[key] = final[key].concat(tag);
            
            return final;
          }, {});

          console.log('Active Tag Filters', tagsBreakdown, this.appliedFilters.tags, products.length);

          products = [...products].filter((item) => {
            return [...Object.keys(tagsBreakdown)].every((key) => {
              return intersection(item.tags, tagsBreakdown[key]).length > 0;
            });
            
            return intersection(item.tags, appliedFilters.tags).length > 0;
          });
        }
        console.log('Amount before sort', products.length);

        if (appliedFilters.sort) {
          products = this.sortItems(products);
        }

        console.log('Returning', products.length);
        return products;
      },
      sortItems(items) {
        const {sort} = this.appliedFilters;

        if (sort === 'low-high') {
          return [...items].sort((productOne, productTwo) => {
            if (productOne.price < productTwo.price) { return -1; }
            if (productOne.price > productTwo.price) { return 1; }

            return 0;
          });
        }

        if (sort === 'high-low') {
          return [...items].sort((productOne, productTwo) => {
            if (productOne.price < productTwo.price) { return 1; }
            if (productOne.price > productTwo.price) { return -1; }

            return 0;
          });
        }

        if (sort === 'in-stock') {
          return [...items].filter((product) => {

            const preOrderTagPresent = product.tags.find((tag) => /preorder/i.test(tag));

            if (!preOrderTagPresent) {
              return product.available;
            }

            const productWidePreorder = (preOrderTagPresent.indexOf(':') === -1);

            console.log('Product', product.title, product.available, !productWidePreorder);

            return !productWidePreorder && product.available;


            const hasPreorderTag = product.tags.find((tag) => {
              return /preorder/ig.test(tag);
            });

            return product.available && !hasPreorderTag;
          });
        }

        if (sort === 'best-selling') {
          return [...items].sort((productOne, productTwo) => this.bestSellingOrder.indexOf(productOne.handle) - this.bestSellingOrder.indexOf(productTwo.handle));
        }

        return items;
      },
      filterSize(items) {
        const {sizes} = this.appliedFilters;

        // const associatedMaskedSizes = window.theme.filter_settings.mask_sizes[data.handle];

        // const sizeWithMasked = sizes.reduce((all, size) => {
        //   const associated = associatedMaskedSizes[size] || [];
        //   return all.concat(associated);
        // });

        // console.log('Filtering Sizes', sizes, associatedMaskedSizes, sizeWithMasked);


        const qualifyingItems = [...items].filter((item) => {
          const {options, variants} = item;
          const sizeOption = [...options].find((option) => /size/i.test(option.name));

          const hasCrossedSize = [...sizes].find((size) => {
            const sizeAvailable = [...variants].find((variant) => variant[`option${sizeOption.position}`] === size);

            return sizeAvailable;
          });

          return Boolean(hasCrossedSize);
        });

        return [...qualifyingItems];
      },
      filterSizeSpecial(items) {
        let {sizes} = this.appliedFilters;

        sizes = [...sizes].filter((size) => Boolean(size.value));
        console.log('aLl Sizes', sizes, this.appliedFilters);

        const qualifyingItems = [...items].filter((item) => {
          const {collections, options, tags, variants} = item;

          const sizeOption = [...options].find((option) => /size/i.test(option.name));

          const qualifiesBySize = [...sizes].find((sizeFilter) => {
            const {testType, value} = sizeFilter;
            // exit if no common collections.

            // console.log('Size Filter', sizeFilter, value);

            if(testType === 'tags') {
              if (intersection(tags, sizeFilter.collections).length === 0) { return false; }
            } else {
              // console.log('Test Type', sizes, testType, collections, sizeFilter, sizeFilter.collections);

              if(testType == 'exclusionCollection') {
                const filterDefinition = [...filterDefinitions].find((item) => item.label === sizeFilter.value);


                console.log('Filter Definition', filterDefinitions, filterDefinition, sizeFilter, sizeFilter.value);
                
              } else {

                if (intersection(collections, sizeFilter.collections).length === 0) { return false; }
              }
            }



            const sizeAvailable = [...variants].find((variant) => variant[`option${sizeOption.position}`] === value);

            return Boolean(sizeAvailable);
          });

          return Boolean(qualifiesBySize);
        });


        return [...qualifyingItems];
      },
      infiniteClick() {
        console.warn('Autoclick Infinite Click');
        const {currentPage} = this.pagination;

        this.pagination.currentPage = currentPage + 1;
      },
      toggleOpenFilters(event) {
        console.log('Toggle Event', event);
        if (event && event.preventDefault) {
          event.preventDefault();
          event.stopPropagation();
        }

        console.log('Toggle Open Filters', event.target);

        if (window.innerWidth <= 786) {
          this.filtersOpen = !this.filtersOpen;
          this.$emit('link-disable', this.filtersOpen);
          this.$emit('filter-toggle', this.filtersOpen);
        }
      },
      updateFilters({add, payload, testType, type, value}) {
        console.log('Update Filters', add, payload, type, value);
        switch (type) {
          case 'sort':
            this.updateSortOption({value});
            break;
          case 'tag':
            this.updateTagFilter({add, value});
            break;
          case 'size':
            (this.standard) ? this.updateSizeFilter({add, value}) : this.updateSpecialSizeFilter({add, payload, testType, value});
            break;
        }

        this.resetInitialPage();
        this.updateBrowserFilterState();
      },
      updateBrowserFilterState() {
        console.log('Updating Browser State', this.appliedFilters);
        updateUrlParameters({
          ...this.appliedFilters,
        });
        // window.history.pushState({
        //   ...this.appliedFilters,
        // }, '', '');
      },
      updateSortOption({value}) {
        this.appliedFilters.sort = (value === 'manual') ? null : value;

        // const shopifySearch = sortAssociated(value);

        updateUrlParameters({
          sort: this.appliedFilters.sort,
        });
      },
      updateSizeFilter({add, value}) {
        console.warn('Regular Size Update', add, value);
        if (add) {
          this.appliedFilters.sizes = [...new Set(this.appliedFilters.sizes.concat(value))];
        } else {
          const currentAppliedSizes = [...this.appliedFilters.sizes];

          this.appliedFilters.sizes = currentAppliedSizes.filter((size) => !value.includes(size));
        }
      },
      updateSpecialSizeFilter({add, payload, testType, value}) {
        if (add) {
          const valueIndex = [...this.appliedFilters.sizes].findIndex((item) => item.value === value);
          if (valueIndex === -1) {
            if(Array.isArray(value)) {

              [...value].forEach((v) => {
                this.appliedFilters.sizes.push({
                  value: v,
                  collections: payload,
                  testType,
                });
              });
            } else {
              this.appliedFilters.sizes.push({
                value,
                collections: payload,
                testType,
              });
            }
          } else {
            const {collections} = [...this.appliedFilters.sizes][valueIndex];

            [...this.appliedFilters.sizes][valueIndex].collections = [...new Set([...collections, ...payload])];
          }
          console.log('Special Size Update', testType, this.appliedFilters.sizes, value);
        } else {
          const valueIndex = [...this.appliedFilters.sizes].findIndex((item) =>{
            if(typeof value === 'string') {
              return item.value === value;
            }

            if(Array.isArray(value)) {
              return value.includes(item.value);
            }
          });

          console.log('Checking Value Index', this.appliedFilters.sizes, valueIndex, value);
          const {collections} = [...this.appliedFilters.sizes][valueIndex];
          const newCollections = collections.filter((handle) => !payload.includes(handle));

          const filters = {...this.appliedFilters};
          filters.sizes[valueIndex] = [...newCollections];
          filters.sizes = filters.sizes.filter((array) => Boolean(array.collections) && Boolean(array.value));

          this.appliedFilters = {...filters};
          console.log('New Sizes after Removal', this.appliedFilters, this.appliedFilters.sizes[valueIndex]);
        }
      },
      updateTagFilter({add, value}) {
        // console.log('Update Tag Filter', add, value);
        if (add) {
          this.appliedFilters.tags.push(value);

          console.log('Applied Tag Filter', this.appliedFilters.tags);
        } else {
          const newTags = [...this.appliedFilters.tags].filter((tag) => tag !== value || tag === '');
          this.appliedFilters.tags = [...newTags];
        }
      },
      resetInitialPage() {
        const currentSearch = window.location.search;

        const newSearch = (/(page).*?(&|$)/gi.test(currentSearch)) ? currentSearch.replace(/(page).*?(&|$)/gi, 'page=1&') : (currentSearch.indexOf('?') > -1) ? `${currentSearch}&page=1` : '?page=1';

        this.pagination.initialPage = 1;
        window.history.pushState(
          {},
          null,
          newSearch,
        );
      },
      scrollTo() {
        const landingParams = getJsonFromUrl();

        function scrollToElement(id) {
          const link = document.querySelector(`#product-${id}`);

          console.log('Scroll To Element', link, id);

          if (!link) {
            return setTimeout(() => {
              scrollToElement(id);
            }, 500);
          }

          const element = link;
          const headerOffset = document.querySelector('header').clientHeight + 20;
          const elementPosition = element.getBoundingClientRect().top;
          const offsetPosition = elementPosition - headerOffset;

          updateUrlParameters({
            i: false,
          });

          window.scrollTo({
            top: offsetPosition,
                // behavior: 'smooth',
          });
          window.scrolled = true;

          console.warn('SCROLL TO', el, id, offsetPosition);
        }

        console.log('Landing page Params', landingParams, (landingParams.page && landingParams.i));

        if (landingParams.page && landingParams.i) {
          const id = landingParams.i;

          scrollToElement(id);
        }
      },
      async storefrontQuery() {
        let after = null;
        let firstLoop = true;
        let items = [];
        const token = window.preview_key;
        const url = `https://doen-2.myshopify.com/api/2021-07/graphql.json`;
        const headers = {
          'Content-Type': 'application/json',
          'X-Shopify-Storefront-Access-Token': token,
        };

        function collectionQuery() {
          return `{
            collectionByHandle(handle:"${data.handle}"){
                  title
                  products(first:21, sortKey:COLLECTION_DEFAULT ${after ? `, after:"${after}"` : ''}) {
                    pageInfo {
                      hasNextPage
                    }
                    edges { 
                      cursor
                      node {
                        availableForSale
                        handle
                        id
                        images(first: 2) {
                          edges {
                            node {
                              altText
                              id
                              originalSrc
                            }
                          }
                        }
                        onlineStoreUrl
                        priceRange {
                          maxVariantPrice {
                            amount
                          }
                          minVariantPrice {
                            amount
                          }
                        }
                        productType
                        options {
                          name
                          values
                        }
                        tags
                        totalInventory
                        title,
                        variants(first: 10) {
                          edges {
                            node {
                              availableForSale,
                              quantityAvailable,
                              selectedOptions {
                                name
                                value
                              }
                            }
                          }
                        }
                      }
                    }
                  }
            }
          }`
        }

        while(after || firstLoop) {
          
          try {
            let response = await fetch(url, {
              method: 'POST',
              headers,
              body: JSON.stringify({
                query: collectionQuery()
              })
            }).then((res) => res.json()).catch((error) => console.log('Storefront error', { error }));
            
            // console.log('Storefront Response', response);

            if(firstLoop) {
              let product = response.data.collectionByHandle.products.edges[0].node;
              // console.log('First Loop!', { response, query: collectionQuery(), product });
              // console.log('Storefront Response', { response, query: collectionQuery() });
            }
            
            let collection = response.data.collectionByHandle;

            // console.log('Has Next Page?', collection);

            if(collection.products.pageInfo.hasNextPage) {
              after = [...collection.products.edges].reverse()[0].cursor;
              await sleep(500);
            } else {
              after = null;
            }
  
            items = items.concat(collection.products.edges);
          } catch (error) {
            // console.log('Storefront Error', { error });
            firstLoop = false;
            after = null;
          }

          // console.log('Loop Storefront',{ after, items});

          firstLoop = false;
        }

        function shapeItem(item) {
          let {
            images,
            title,
            priceRange,
            options,
            variants,
          } = item.node;

          priceRange = [...Object.keys(priceRange)].reduce((range, key) => {
            range[key] = {
              ...priceRange[key],
              amount: Number(priceRange[key].amount) * 100,
            };

            return range;
          }, {});

          return {
            ...item.node,
            images: images ? images.edges.map((edge) => {
              return {
                ...edge.node,
              }
            }) : [],
            options: [...options].map((option, index) => {
              return {
                ...option,
                position: index+1,
              }
            }),
            variants: [...variants.edges].map((edge) => {
              let variant = edge.node;

              let options = [...variant.selectedOptions].reduce((options, option, index) => {
                options[`option${index+1}`] = option.value;

                return options;
              }, {});
              return {
                ...variant,
                inventory_quantity: variant.quantityAvailable,
                options: [...variant.selectedOptions].map((option) => option.name),
                ...options,
                preorder: {
                  limit: 0,
                },
              }
            }),
            priceRange,
            title: title.split(' -- ')[0],
          }
        }

        // console.log('One Item', items[0]);

        return items.map(shapeItem);
      },
      async storefrontPreview() {
        // console.log('Storefront Preview');

        let items = await this.storefrontQuery();

        this.allProducts = [...items];
        this.gatherFinished = true;
        this.previewLoading = false; 
        this.pagination.maxPages = Math.ceil(this.pagination.maxPages / this.pagination.perPage);
        // console.log('Items', { items }, items[0]);
      },
    },
    created() {
      console.log('Filters Created');
    },
    async mounted() {

      console.log('Mounted with preview?', window.preview_mode);

        
        

      if(window.preview_mode) {
        // preview mode ( Storefront API Driven ).
        this.previewLoading = true;
        this.storefrontPreview();

        this.allProducts = []; 
      } else {
        // Normal, non-preview mode.

        // await this.fetchPage(1);
        const pages = Math.ceil(data.total_count / 40);
        const pageArray = Array.from(Array(pages).keys()).map((n) => n + 1);
  
        // Update filters from state.
  
        // console.log('Filters State after History', this.appliedFilters);
  
        this.fetchBestSellingOrder();  
        await Promise.all(pageArray.map(this.fetchPage));
  
        // console.log('After all items');
        this.allProducts = [...this.tempItems];
        this.gatherFinished = true;
      }

      document.getElementById('currency').addEventListener('change', (event) => {
        // console.log('Currency Changed', event.target.value);

        this.currency = event.target.value;
      });

      window.addEventListener('popstate', (event) => {
        // console.log('Window Popstate! Force Update');
        this.scrollTo();
      });

      // console.log('COMPLETED', this.allProducts);
    },
  });

  // console.log('Collection Pages');
})();

