(function(){
    function loadImg($img) {
        const src = $img.getAttribute('data-src');
        $img.setAttribute('src', src);
        $img.removeAttribute('data-src');
    }

    function setImageSize($img) {
        const $temp = new Image();
        const src = $img.getAttribute('data-src');
        $temp.src = $img.getAttribute('data-src');

        $temp.onload = function() {
            const width = $temp.naturalWidth;
            const height = $temp.naturalHeight;
            const tWidth = $img.width;

            const neededHeight = Math.round((height/ width) * tWidth);

            if(neededHeight !== 0 && window.innerWidth > 768) {
                $img.setAttribute('height', neededHeight);
            }

            var resizeTimer;

            $(window).on('resize', function(e) {

            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                const width = $temp.naturalWidth;
                const height = $temp.naturalHeight;
                const tWidth = $img.width;
    
                const neededHeight = Math.round(((height/ width) * tWidth)) + 1;
    
                if(neededHeight !== 0 && window.innerWidth > 768) {
                    $img.setAttribute('height', neededHeight);
                }
                // Run code here, resizing has "stopped"
                        
            }, 250);

            });
        }

    }

    function lazyLoads() {
        const $imgs = document.querySelectorAll('.lazy-item');
        let observer;
      
        let options = {
          root: null,
          rootMargin: "0px",
          threshold: 0.1
        };

        function handleLazyIntersect(entries, observer) {
            entries.forEach((entry) => {
                if(entry.isIntersecting) {
                    observer.unobserve(entry.target);
                    loadImg(entry.target);
                }
            });
        }
      
        observer = new IntersectionObserver(handleLazyIntersect, options);


        [...$imgs].forEach(function($img) {
            setImageSize($img);
            observer.observe($img);
        });

    }

    function tabs() {
        const $btns = document.querySelectorAll('.toc-item button');
        const $panels = document.querySelectorAll('.resolutions-panel')

        function clickHandler(event) {
            const $target = event.currentTarget;
            const controls = $target.getAttribute('aria-controls');

            [...$panels].forEach(function($panel) {
                const id = $panel.id;

                if(id === controls) {
                    $panel.classList.add('active');
                } else {
                    $panel.classList.remove('active');
                }
            });

            var element = document.querySelector('.resolutions-panel.active');
            var headerOffset = document.querySelector('header.site-header').offsetHeight;
            var elementPosition = element.getBoundingClientRect().top;
            var offsetPosition = elementPosition + window.pageYOffset - headerOffset;
                    
            window.scrollTo({
                    top: offsetPosition,
                    behavior: "smooth"
            });

        }

        [...$btns].forEach(function($btn, index) {
            $btn.addEventListener('click', clickHandler);
        });
    }


    tabs();
    lazyLoads();
})();