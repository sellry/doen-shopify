(function() {

    const loadImage = function(img) {
        const src = img.getAttribute('data-src');
        img.setAttribute('src', src);
        img.classList.remove('lazy-img');
        img.classList.add('observed');
    };
  
    const imageIntersectionCallback = function(entries, observer) {
          const intersecting = entries.filter(function(entry) {
              return entry.isIntersecting;
          });
  
          intersecting.forEach(function(entry){
              loadImage(entry.target);
          });
    };

    const assignImageObservers = function() {
        const options = {
            threshold: 0.1,
            root: null,
            rootMargin: `0px 0px 0px 0px`
        };          

        const observer = new IntersectionObserver(imageIntersectionCallback, options);
        const $imgs = document.querySelectorAll('.lazy-img');



        console.warn('Assign Observers!', $imgs);
  
        $imgs.forEach(function(img){
            if (!img.classList.contains('observed')) {
                observer.observe(img);
                img.classList.add('observed');
            }
        });
    };

    assignImageObservers();

    window.lazyLoadObservers = assignImageObservers;
    window.lazyLoadImage = loadImage;
})();