console.warn('Product 404 feed');

(function(){
	const url  = `/collections/${window.feed_source}?page=3`;
  	const gridItems = document.querySelectorAll('.collection-infinite .product-infinite');
  
  const fetchNewItems = function(){
  	return fetch(url)
    .then(function(res){ return res.text() })
  };
  
  const getNewItems = async function(){
  	const newItems = await fetchNewItems();
    const newHTML = $(newItems).find('.collection-infinite .product-infinite');
    
    newHTML.each(function(index, item){
      $('.collection-infinite').append(item);
    });
    window.lazyLoadObservers();
    console.warn('Fetched', newHTML);
  };
  
  if (gridItems.length < 10) {
    	console.warn('Fetch');
    
    getNewItems();
  }
})();