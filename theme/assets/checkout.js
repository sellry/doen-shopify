(function(){
    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }
  
    const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

    function getCart() {
        return fetch(`/cart.js`).then((res) => res.json());
    }

    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    const donationAdjustments = () => {
        const $tipWrapper = document.querySelector('#section--tip-presets');

        // if the content isn't on the page yet, just reset.
        if(!$tipWrapper) return setTimeout(donationAdjustments, 300);

        const $donationButtons = $tipWrapper.querySelectorAll('button[name="checkout[amount_tip]"]');

        [...$donationButtons].forEach(($btn) => {
            let $content = $btn.querySelector('.btn__content');
            let amount = /$/ig.test($content.textContent) ? `${Number($content.textContent.replace('$', ''))}.00` : 0.00;

            $btn.setAttribute('value', amount);
        })
    }

    const orderNote = async () => {
        const $tipWrapper = document.querySelector('.section.section--tip');
      	const $sectionNote = document.querySelector('.section--note');
      
        if($sectionNote) return;

        // if the content isn't on the page yet, just reset.
        if(!$tipWrapper) return setTimeout(orderNote, 300);

        const cart = await getCart();
        let $section = document.createElement('div');

        console.log('Cart', { cart });

        $section.classList.add('section');
        $section.classList.add('section--note');

        $section.innerHTML = `
        <div class="section__header">
          <h2 class="section__title" id="note_title">Add a Gift Note</h2>
        </div>
        <div class="section__content">
            <div class="content-box">
              <div class="">
                <form class="edit_checkout animate-floating-labels" accept-charset="UTF-8" method="POST">
                    <div class="field field--with-prefix field--show-floating-label">
                        <div class="field__input-btn-wrapper">
                         
                            <div class="field__input-wrapper">
                              <label class="field__label field__label--visible" for="section--note-input"></label>
                              <span class="field__prefix">$</span>
                              <input
                                class="field__input" 
                                id="section--note-input" 
                                type="text"
                                name="checkout[note]"
                              >
                            </div>

                        </div>                    
                    </div>
                </form>
              </div>
            </div>
        </div>
        `;

        function handleKeyDown(event) {
            console.log('Final Debounce');
            const note = event.target.value;

            jQuery.post('/cart/update.js', { note }, function(data) {
                window.location.reload();
            });
        }

        $section.querySelector('input').addEventListener('keydown', debounce(handleKeyDown, 500))
        $section.querySelector('input').value = cart.note || '';

        console.log('Insert', { $section, $tipWrapper });

        insertAfter($section, $tipWrapper);
    };

    const shippingMessageAtPayment = () => {
        let $section = document.createElement('div');
        let $onPageSection = document.querySelector('.section--reductions.hidden-on-desktop');

        $section.classList.add('section');
        $section.classList.add('section--shipping-msg');

        $section.innerHTML = `
        
        <div class="section__content">
            <div class="content-box">
                <p>${window.checkout_shipping_msg.message}</p>
            </div>
        </div>
        `;

        insertAfter($section, $onPageSection);
        console.log('Shipping Message At Payment');
    }
  
    const autoSelectFreeShipping = async () => {
      let $shippingMethods = document.querySelector('.content-box[data-shipping-methods]');

      if(!$shippingMethods) return setTimeout(autoSelectFreeShipping, 500);
      
      // additional timeout for the dom to populate
      await sleep(250);

      const $fedexGrndSpan = $shippingMethods.querySelector('label .radio__label__primary[data-shipping-method-label-title*="Priority Mail"]');
	  
      if(!$fedexGrndSpan) return setTimeout(autoSelectFreeShipping, 500);
      
      const $fedexLabel = $fedexGrndSpan.parentElement;
      const $fedexPrice = $fedexLabel.querySelector('.content-box__emphasis');
      
      const fedexIsFree = /free/i.test($fedexPrice.textContent)
      
      if(fedexIsFree) $fedexLabel.click();
     
    };

    const stepFunctions = {
        payment_method: [donationAdjustments, orderNote],
        shipping_method: [autoSelectFreeShipping]
    }

    if(window.checkout_shipping_msg && window.checkout_shipping_msg.message) {
        stepFunctions.payment_method.push(shippingMessageAtPayment);
    }
    
    const start = () => {
        let step = window.Shopify.Checkout.step;

        if(stepFunctions[step]) {
            [...stepFunctions[step]].forEach((fn) => fn());
        }
        // donationAdjustments();
        // orderNote();
    };

    document.addEventListener('page:change', () => {
        start()
    });

    start();
    console.log('Checkout Script Adjustments');
})();