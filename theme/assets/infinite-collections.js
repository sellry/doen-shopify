var pInfScrLoading = false;
var pInfScrDelay = 250;

if ('scrollRestoration' in history) { history.scrollRestoration = 'manual'; }



// This is the main function
// It has been removed from the doc.ready function because
// we want to be able to run the same function for safari back functions (when it loads from cache).

function run() {
  
  console.log('This isthe infinite collections');

	attachClickEvent();
	loadPagesWhenGoingBack();

// 	$(window).scroll(function(){
// 		$.doTimeout( 'scroll', pInfScrDelay, function() {
// 			if (!pInfScrLoading && $(document).height()-$('footer').height()-$(window).height()-$(document).scrollTop() < 400) {
// 				pInfScrLoading = true;
// 				pInfScrExecute();
// 			}
// 		});
// 	});


	function pInfScrExecute() {
		pInfScrNode = $('.more-button').last();	
		pInfScrURL = $('.more-button a').last().attr("href");
		if (pInfScrNode.length > 0) {
			if (pInfScrNode.css('display') != 'none') {
				$.ajax({
					type: 'GET',
					url: pInfScrURL,
					beforeSend: function() {
						pInfScrLoading = true;
						// pInfScrNode.clone().addClass('spinner').empty().insertAfter(pInfScrNode).append('<img src=\"http://cdn.shopify.com/s/files/1/0068/2162/assets/loading.gif?105791\" />');
						// pInfScrNode.hide();
					},
					success: function(data) {
						pInfScrNode.remove();
						var filteredData = $(data).find(".collection-infinite");
						if( filteredData.find('.product-infinite').length == 0 ){
							  $(window).trigger('scroll');
						}
						const items = filteredData.find('.product-infinite');
                      	const $money = filteredData.find('span.money');
                      	$money.each(function() {
                          $(this).attr('data-currency-USD', jQuery(this).html());
                          $(this).attr('data-currency', 'USD');
                          
                          const amt = parseInt($(this).html().replace('$',''));
                          const converted = Currency.convert(amt, 'USD', Currency.currentCurrency);
                          const formatted = Currency.formatMoney(converted*100, Currency.moneyFormats[Currency.currentCurrency].money_with_currency_format );
                           $(this).attr('data-currency', Currency.currentCurrency);
                          $(this).html( formatted );
                        });

						console.log(filteredData.find('.product-infinite').length, 'ITEMS', items);
						$('.collection-infinite').append( filteredData.children() );
                     	// $('#currency').trigger('change');
                      
						pInfScrLoading = false;
						attachClickEvent();
					},
					dataType: "html"
				});
			}
		} else {
			$('.inf-collection-loading').addClass('hidden');
		}
	}

	function pInfScrExecuteBack(remainingPages, done) {
      console.log('going to try', $('.more-button a').last().attr("href"), remainingPages);
		if (remainingPages == 0) return done();

		pInfScrNode = $('.more-button').last();	
		pInfScrURL = $('.more-button a').last().attr("href");
		if (pInfScrNode.length > 0) {
			if (pInfScrNode.css('display') != 'none') {
				$.ajax({
					type: 'GET',
					url: pInfScrURL,
					beforeSend: function() {
						pInfScrLoading = true;
					},
					success: function(data) {
						pInfScrNode.remove();
						var filteredData = $(data).find(".collection-infinite");
// 						filteredData.insertAfter( $('.collection-infinite').last() );
                      $('.collection-infinite').append(filteredData.children());
						pInfScrLoading = false;
						attachClickEvent();

						if(remainingPages > 1) {
							pInfScrExecuteBack(remainingPages-1, done);
						} else {
							done();
						}
					},
					dataType: "html"
				});
			}
		} else {
			$('.inf-collection-loading').addClass('hidden');
		}
	}

	function attachClickEvent(){
		$('li.more-button a').click(function(event){
			pInfScrExecute();
			event.stopPropagation();
			return false;
		});
	}


	$(document).on("click", ".collection-infinite li div a", function(e){
		e.preventDefault();
		var productId = $(this).data("product-id");
		var page = $(this).data('page');
		history.pushState({}, "", "#/"+ page +"/" + productId);
		window.location.href = $(this).attr('href');
	});

	function loadPagesWhenGoingBack(){
		if (window.location.hash) {
			var params = window.location.hash.split('/');
			var pagesToLoad = params[1];
			var scrollToItem = params[2];
          
          console.warn('Load Pages when going back', pagesToLoad);

			pInfScrExecuteBack(pagesToLoad-1, function() {
              
              console.log('Its Done');
              $('span.money').each(function() {
                if($(this).attr('data-currency') !== 'USD'){ // skip if currency already converted
                  	return true;
                }
                          $(this).attr('data-currency-USD', jQuery(this).html());
                          $(this).attr('data-currency', 'USD');
                          
                          const amt = parseInt($(this).html().replace('$',''));
                          const converted = Currency.convert(amt, 'USD', Currency.currentCurrency);
                          const formatted = Currency.formatMoney(converted*100, Currency.moneyFormats[Currency.currentCurrency].money_with_currency_format );
                           $(this).attr('data-currency', Currency.currentCurrency);
                          $(this).html( formatted );
             });
              
              console.warn('Scroll To', scrollToItem);
                window.lazyLoadObservers(); 
				goToProductPosition(scrollToItem);
			});
		}
	}

	function goToProductPosition(productId){ 
		var productDiv = $('#product-' + productId);
      
      console.log('Img', productDiv.find('img'));
      setTimeout(function(){
		var topPosition = productDiv.offset().top-53;
      	console.warn('New Position', productDiv.offset().top);
        		$('html,body').animate({scrollTop: topPosition}, 'slow');
      console.warn('Scroll to Product', productId, productDiv, topPosition, $(document).height());
      }, 500);

	}
  
  if( $('.product-infinite').length === 0 ){
    $(window).scroll();
    setTimeout(function(){
    	$(window).scroll();
    }, 500);
  }

}


// This is for mac safari that will force a reload on 'back' instead of loading the page from cache.
$(window).bind("pageshow", function(event) {
  console.log('Window pageshow');
  if (event.originalEvent.persisted) {
//     window.location.reload() 
    run();
  }
});

$(document).ready(function () {
  run();
});




