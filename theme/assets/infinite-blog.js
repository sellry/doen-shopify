var pInfScrLoading = false;
var pInfScrDelay = 250;

if ('scrollRestoration' in history) { history.scrollRestoration = 'manual'; }

$(document).ready(function () {

	attachClickEvent();
  	loadPagesWhenGoingBack();

	$(window).scroll(function(){
		$.doTimeout( 'scroll', pInfScrDelay, function() {
			if (!pInfScrLoading && $(document).height()-$('footer').height()-$(window).height()-$(document).scrollTop() < 400) {
				pInfScrLoading = true;
				pInfScrExecute();
			}
		});
	});

	function pInfScrExecute() {
		pInfScrNode = $('.more-button').last();	
		pInfScrURL = $('.more-button a').last().attr("href");
		if (pInfScrNode.length > 0) {
			if (pInfScrNode.css('display') != 'none') {
				$.ajax({
					type: 'GET',
					url: pInfScrURL,
					beforeSend: function() {
						pInfScrLoading = true;
					},
					success: function(data) {
                      if(data){
                        console.log("new page loaded!");
                        console.log(data);
                      	}
						// pInfScrNode.remove();
						var filteredData = $(data).find(".product-infinite");
                      var link = $(data).find('.more-button a');
                      var clearFix = $("<div>", { class: "clearfix added" });
                      var newLink = 
                      // clearFix.insertAfter( $('.collection-infinite').last() );
                      filteredData.each(function(){
                      	var price = $(this).find('.money');
                        var currentPrice = price.text();
                        price.attr(`data-currency-usd`, currentPrice);
                        var converted = Currency.convert(parseInt(currentPrice.replace('$','')),'USD',Currency.currentCurrency);
                        price.attr(`data-currency-${Currency.currentCurrency}`,Currency.formatMoney(parseInt(currentPrice.replace('$',''))*100, Currency.moneyFormats[`${Currency.currentCurrency}`].money_with_currency_format));
                        price.text(Currency.formatMoney(converted*100, Currency.moneyFormats[`${Currency.currentCurrency}`].money_with_currency_format));
                        console.log(Currency.formatMoney(parseInt(currentPrice.replace('$',''))*100, Currency.moneyFormats[`${Currency.currentCurrency}`].money_with_currency_format), 'PRICE', price, currentPrice, converted);
                      });
						filteredData.insertAfter( $('.product-infinite').last() );
                        // link.insertAfter( $('.product-infinite').last() );
                      
                      if(link.length > 0 ){
                        var $href = link.attr('href');
                        $('.more-button a').attr('href', $href);
                      }else{
                        $('.more-button').remove();
                      }
                      
                      setTimeout(function(){
                      	$('.currency-picker').trigger('change');
                      }, 300);
						pInfScrLoading = false;
						attachClickEvent();
					},
                  complete: function(){
                    	$('.currency-picker').trigger('change');
                  },
					dataType: "html"
				});
			}
		} else {
			$('.inf-collection-loading').addClass('hidden');
		}
	}

	function pInfScrExecuteBack(remainingPages, done) {
		if (remainingPages == 0) return done();

		pInfScrNode = $('.more-button').last();	
		pInfScrURL = $('.more-button a').last().attr("href");
		if (pInfScrNode.length > 0) {
			if (pInfScrNode.css('display') != 'none') {
				$.ajax({
					type: 'GET',
					url: pInfScrURL,
					beforeSend: function() {
						pInfScrLoading = true;
					},
					success: function(data) {
						pInfScrNode.remove();
						var filteredData = $(data).find(".collection-infinite");
						filteredData.insertAfter( $('.collection-infinite').last() );
						pInfScrLoading = false;
						attachClickEvent();
 
						if(remainingPages > 1) {
							pInfScrExecuteBack(remainingPages-1, done);
						} else {
							done();
						}
					},
					dataType: "html"
				});
			}
		} else {
			$('.inf-collection-loading').addClass('hidden');
		}
	}
  
  	function loadPagesWhenGoingBack(){
		if (window.location.hash) { 
			var params = window.location.hash.split('/');
			var pagesToLoad = params[1];
			var scrollToItem = params[2];
          
          console.warn('Load Pages when going back', pagesToLoad, scrollToItem);

			pInfScrExecuteBack(pagesToLoad-1, function() {
				goToProductPosition(scrollToItem);
			});
		}
	}

	function goToProductPosition(productId){ 
		var productDiv = $('#article-' + productId);
		var topPosition = Math.round(productDiv.offset().top-53);
		$('html,body').animate({scrollTop: topPosition}, 'slow');

		setTimeout(function(){
			if (topPosition < Math.round(productDiv.offset().top-53) ) {
				goToProductPosition(productId);
			}
		}, 650);
	}

	function attachClickEvent(){
		$('li.more-button a').click(function(event){
          console.log("Exists");
			pInfScrExecute();
			event.stopPropagation();
			return false;
		});
	}


	$(document).on("click", ".collection-infinite li a", function(e){
			e.preventDefault();
			var productId = $(this).data("product-id");
			var page = $(this).data('page');
			const href = $(this).attr('href');
			history.pushState({}, "", "#/"+ page +"/" + productId);
			window.location.href = $(this).attr('href');
	});

});




