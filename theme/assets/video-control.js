(function(){
    const videoWrappers = document.querySelectorAll('.video-section-wrapper iframe');

    let observer = new IntersectionObserver((entries, observer) => { 
        entries.forEach(entry => {
            const vimeo = entry.target.player;
        if(entry.isIntersecting){
            vimeo.play();
        } else {
            vimeo.pause();
        }
        });
    }, {rootMargin: "0px 0px -200px 0px"});
 

    document.querySelectorAll('.video-section-wrapper iframe').forEach($video => { 
        // Set Observer
        const autoplay = $video.getAttribute('data-autoplay');
        if (autoplay === 'true') {
            observer.observe($video);
        }

        const player = new Vimeo.Player($video, {
            loop: true,
            controls: false,
        });
        $video.player = player;
    });


    console.warn('Video Controllers!', videoWrappers);
})();